<%-- 
    Document   : index
    Created on : 08-05-2021, 13:43:00
    Author     : Walter
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.entity.TblHistorialDefiniciones"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String definicion = request.getAttribute("definicion").toString();
    //List<TblHistorialDefiniciones> tblHistorialDefiniciones = (List<TblHistorialDefiniciones>) request.getAttribute("tblHistorialDefiniciones");
    //Iterator<TblHistorialDefiniciones> iteradorDefiniciones = tblHistorialDefiniciones.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definición</title>
    </head>
    <body>
        <div>
            <div>
                <div>
                    <h2>Diccionario</h2>
                    <h5>Examen | Bryan Walter | sección 50</h5>
                    <hr>
                    <h6><strong>Definición encontrada:</strong></h6>
                    <hr>
                    <p><%= definicion%></p>
                    <hr>
                    <input type="button" value="Volver" onclick="window.history.back();">
                </div>
            </div>
        </div>
        <p></p>
    </body>
</html>
