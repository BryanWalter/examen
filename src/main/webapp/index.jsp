<%-- 
    Document   : index
    Created on : 08-05-2021, 13:43:00
    Author     : Walter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inicio</title>
        
    </head>
    <body>
        <div>
            <div>
                
                <div>
                    <h2>- Diccionario</h2>
                    <h5>Examen | Bryan Walter | sección 50</h5>
                    <hr>
                    <h5> URL HEROKU:https://examen-final-bw.herokuapp.com/ </h5>
                    <h5> URL GET:https://examen-final-bw.herokuapp.com/api/definiciones </h5>
                    <h5> URL POST:https://examen-final-bw.herokuapp.com/api/definiciones/{palabra} </h5>
                    <form name="frm_consulta" action="miControlador" method="POST">
                        <div>
                            <label for="txt_palabra">Ingrese una palabra:</label>
                            <div>
                                <input type="text" name="txt_palabra" placeholder="Ej: Perro" class="form-control">
                            </div>
                        </div>
                        <hr>
                        <input type="submit" name="btn_consultar" value="Consultar definición">
                        <input type="submit" name="btn_historial" value="Ver historial de búsquedas">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
